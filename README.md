#### 介绍

基于桑帛云api的一些js插件

>  **作者联系方式（qq）：1283411677** 


#### 安装教程

 js文件放入/plugins/example/内即可

#### 注意事项

-  **在安装使用js之前，请务必详细阅读js中注释！！！**
-  ai绘图插件未完成绘图之前请勿频繁调用
-  插件问题请在此页面的issues中提交或联系作者本人说明
-  **请不要在api反馈群中反馈插件问题**

#### 使用说明
| 插件 | 使用方法 | 是否需要依赖/依赖名 | 有无使用条件 | 目前是否可用 |
|---|---|---|---|---|
| ChatGPT | 发送 “#gpt帮助” 查看具体使用方法 | 否/无 | 自备openai账号 或 白嫖使用(需要配置ffmpeg) | $\color{#FF0000}{仅白嫖系列可用}$ |
| 随机图片 |  发送 “#桑帛帮助” 或者 “#sb帮助” 查看具体使用方法（不是我骂人，他缩写就是这样） | 否/无 | 否 | $\color{#FF0000}{小部分可用}$ |
| ai绘图 |  发送 “#ai绘图帮助”  查看具体使用方法 | 是/依赖名：$\color{#FF0000}{ccz}$ | 否 | $\color{#FF0000}{已失效}$ |
| 原神语音生成 |  发送 “#语音帮助”  查看具体使用方法 | 否/无 | 需要配置ffmpeg | $\color{#FF0000}{可用}$ |

#### 依赖安装方法

在云崽根目录打开终端，输入并回车
> `pnpm install 依赖名 -w`

依赖名请查看上方表格

#### 交流与反馈

感谢桑帛云api，官网：https://api.lolimi.cn/

-  **桑帛云api反馈群：810886009** 
-  **请不要在api反馈群中或与其他人反馈插件问题**
-  **此群仅用于反馈api问题（就是插件没有问题，你的参数也没缺但是用不了）**

插件反馈：
 **请联系作者或提交issues** 



