import plugin from '../../lib/plugins/plugin.js';
import {
	segment
} from "oicq";
import cfg from '../../lib/config/config.js';
import common from '../../lib/common/common.js';
import fetch from 'node-fetch';
import fs from "fs";
import moment from "moment";
import settimeoutP from "ccz";

//桑帛api官网：https://api.lolimi.cn/

const key = ''

//桑帛api的使用key，获取地址：https://api.lolimi.cn/user/key
//免费
//网站进不去的话，反馈群：810886009
//网站内注册账号之后，点击控制台，密钥管理
//例如 const key = 'NN9APOGd36mPkpcKsndd85GRgk'

//没有key，插件不会工作
//动动您的小手，毕竟注册账号获取key不需要超过三分钟

export class aidrawapi extends plugin {
	constructor() {
		super({
			name: 'ai绘图4宫格',
			dsc: 'ai绘图js',
			event: 'message',
			priority: 999,
			rule: [{
				reg: `^#ai绘图帮助$`,
				fnc: 'aidrawhelp'
			}, {
				reg: `^#ai绘图(.*)$`,
				fnc: 'aidraw'
			}, ]
		})
	}

	async aidraw(e) {
		if (key == '') {
			e.reply('检测到ai绘图js未填写key\n请打开js详细阅读教程！');
			return true;
		}
		let msg = e.msg.replace(/^#ai绘图/, '');
		if (msg == '') {
			e.reply('画什么？画空气吗？');
			return true;
		}
		if (!msg.includes('3d') || !msg.includes('3D')) msg = msg + '，2D'
		console.log('绘图：' + msg);
		let askidurl = `https://api.lolimi.cn/api/ai/mj1?key=${key}&msg=${msg}`;
		let idres = await fetch(askidurl)
			.catch((err) => logger.error(err));
		idres = await idres.json();
		if (idres.code != 1) {
			e.reply(`api出错或提供的关键词含有敏感词`);
			return true;
		}
		let aiid = idres.data;
		let getpicurl = `https://api.lolimi.cn/api/ai/mj2?key=${key}&id=${aiid}`;
		let picres;
		e.reply(`在画了在画了\n请等待1~3分钟左右~`);
		for (let i = 0;; i++) {
			await settimeoutP.timeoutP(10000);
			picres = await fetch(getpicurl);
			picres = await picres.json();
			if (picres.data != '100%') {
				picres.imageurl = '';
				console.log(picres)
				continue;
			} else break;
			if (i == 40) {
				e.reply('绘图失败请重试！');
				e.reply('最后一次返回值为：\n' + picres);
				return true;
			}
		}
		let picurl = picres.imageurl.replace(/\\/g, '');
		if (!picurl) {
			e.reply(`绘制成功但是图片打包失败\napi疑似出错，请加群反馈`);
			return;
		}
		e.reply([segment.at(e.user_id), segment.image(picurl)]);
		return true
	}

	async aidrawhelp(e) {
		let msg = `指令"#ai绘图"+关键词\n例如"#ai绘图猫"\n响应比较慢，毕竟是api`;
		e.reply(msg);
		return true;
	}
}