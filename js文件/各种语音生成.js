//注意！！！此插件的使用需要配备ffmpeg
//直接百度搜索ffmpeg配置教程即可，需要配置环境变量

import plugin from '../../lib/plugins/plugin.js';
import {
	segment
} from "oicq";
import cfg from '../../lib/config/config.js';
import common from '../../lib/common/common.js';
import fetch from 'node-fetch';
import fs from "fs";
import moment from "moment";

//桑帛api官网：https://api.lolimi.cn/

let speakermap = "空,荧,派蒙,纳西妲,阿贝多,温迪,枫原万叶,钟离,荒泷一斗,八重神子,艾尔海森,提纳里,迪希雅,卡维,宵宫,莱依拉,赛诺,诺艾尔,托马,凝光,莫娜,北斗,神里绫华,雷电将军,芭芭拉,鹿野院平藏,五郎,迪奥娜,凯亚,安柏,班尼特,琴,柯莱,夜兰,妮露,辛焱,珐露珊,魈,香菱,达达利亚,砂糖,早柚,云堇,刻晴,丽莎,迪卢克,烟绯,重云,珊瑚宫心海,胡桃,可莉,流浪者,久岐忍,神里绫人,甘雨,戴因斯雷布,优菈,菲谢尔,行秋,白术,九条裟罗,雷泽,申鹤,迪娜泽黛,凯瑟琳,多莉,坎蒂丝,萍姥姥,罗莎莉亚,留云借风真君,绮良良,瑶瑶,七七,奥兹,米卡,夏洛蒂,埃洛伊,博士,女士,大慈树王,三月七,娜塔莎,希露瓦,虎克,克拉拉,丹恒,希儿,布洛妮娅,瓦尔特,杰帕德,佩拉,姬子,艾丝妲,白露,星,穹,桑博,伦纳德,停云,罗刹,卡芙卡,彦卿,史瓦罗,螺丝咕姆,阿兰,银狼,素裳,丹枢,黑塔,景元,帕姆,可可利亚,半夏,符玄,公输师傅,奥列格,青雀,大毫,青镞,费斯曼,绿芙蓉,镜流,信使,丽塔,失落迷迭,缭乱星棘,伊甸,伏特加女孩,狂热蓝调,莉莉娅,萝莎莉娅,八重樱,八重霞,卡莲,第六夜想曲,卡萝尔,姬子,极地战刃,布洛妮娅,次生银翼,理之律者,真理之律者,迷城骇兔,希儿,魇夜星渊,黑希儿,帕朵菲莉丝,天元骑英,幽兰黛尔,德丽莎,月下初拥,朔夜观星,暮光骑士,明日香,李素裳,格蕾修,梅比乌斯,渡鸦,人之律者,爱莉希雅,爱衣,天穹游侠,琪亚娜,空之律者,终焉之律者,薪炎之律者,云墨丹心,符华,识之律者,维尔薇,始源之律者,雷电芽衣,雷之律者,苏莎娜,阿波尼亚,陆景和,莫弈,夏彦,左然";

export class voicecreate extends plugin {
	constructor() {
		super({
			name: '语音生成',
			dsc: '语音生成',
			event: 'message',
			priority: 999,
			rule: [{
				reg: `^#(语音|生成)帮助$`,
				fnc: `voicecrehelp`
			}, {
				reg: `^#(.*)一言$`,
				fnc: `hitokotosend`
			}, {
				reg: `^#(.*)语音(生成)?(.*)$`,
				fnc: 'voicesend'
			}, {
				reg: `^#(家人们(,|，)?|麦克(阿瑟)?|晓(辰|涵|墨)|派大星|广西老表)(.*)$`,
				fnc: 'voicepro'
			}, ]
		})
	}

	async voicesend(e) {
		let speaker = e.msg.replace(/语音(生成)?(.*)$/g, '')
			.replace(/#/g, '');
		let test = await this.detectspeak(speaker);
		if (test != 1) {
			e.reply(`未知的说话人，发送"#语音帮助"查看列表`);
			return true;
		}
		let msg = e.msg.replace(/^#(.*)语音(生成)?/g, '');
		let geturl = `https://api.lolimi.cn/API/yyhc/y.php?msg=${msg}&speaker=${speaker}`;
		let res = await fetch(geturl);
		res = await res.json();
		if (!res.music) {
			e.reply(`返回值错误，若确保您的参数无错，请加群反馈`);
			return true;
		}
		e.reply(segment.record(res.music));
		return true;
	}

	async hitokotosend(e) {
		let speaker = e.msg.replace(/一言$/g, '')
			.replace(/^#/g, '');
		let test = await this.detectspeak(speaker);
		if (test != 1) {
			e.reply(`未知的说话人，发送"#语音帮助"查看列表`);
			return true;
		}
		let hitokotourl = `https://v1.hitokoto.cn/`;
		let hitokotores = await fetch(hitokotourl);
		hitokotores = await hitokotores.json();
		if (!hitokotores.hitokoto) {
			e.reply(`一言获取错误，请重试`);
			return true;
		}
		let url = `https://api.lolimi.cn/API/yyhc/y.php?msg=${hitokotores.hitokoto}&speaker=${speaker}`;
		let res = await fetch(url);
		res = await res.json();
		if (!res.music) {
			e.reply(`返回值错误，若确保您的参数无错，请加群反馈`);
			return true;
		}
		e.reply(segment.record(res.music));
		e.reply([segment.at(e.user_id), `\n`, hitokotores.hitokoto]);
		return true;
	}

	async voicepro(e) {
		let msg = e.msg.replace(/^#(家人们(,|，)?|麦克(阿瑟)?|晓(辰|涵|墨)|派大星|广西老表)/g, '');
		if (e.msg.startsWith(`#家人们`)) msg = `家人们，` + msg;
		let detmsg = msg.replace(/,|，|\.|。|？|\?|；|：|;|:/g, '');
		if (detmsg.trim()
			.length < 5) {
			e.reply(`请至少输入五个字`);
			return true;
		}
		let n = 1;
		if (e.msg.startsWith(`#家人们`)) n = 1;
		else if (e.msg.startsWith(`#晓辰`)) n = 2;
		else if (e.msg.startsWith(`#晓涵`)) n = 3;
		else if (e.msg.startsWith(`#晓墨`)) n = 4;
		else if (e.msg.startsWith(`#派大星`)) n = 5;
		else if (e.msg.startsWith(`#广西老表`)) n = 6;
		else if (e.msg.startsWith(`#麦克`)) n = 7;
		let geturl = `https://api.lolimi.cn/api/yyhc/tvb?msg=${msg}&n=${n}`;
		let res = await fetch(geturl);
		res = await res.json();
		if (!res.music) {
			e.reply(`返回值错误，若确保您的参数无错，请加群反馈`);
			return true;
		}
		e.reply(segment.record(res.music));
		return true;
	}

	async voicecrehelp(e) {
		/**if (key == '') {
			e.reply(`检测到未填写key\n无法使用本插件`);
			return true;
		}*/
		let msg = `#"角色名"语音(生成)xxx\n` +
			`#"角色名"一言\n` +
			`*#[家人们/晓辰/晓涵/晓墨/派大星/广西老表/麦克阿瑟]xxx`
		let tip = `"()"代表可不加\n` +
			`xxx代表任意内容\n` +
			`number代表自然数\n` +
			`/代表“或者”\n` +
			`*代表功能暂时失效\n` +
			`[]可以直接无视\n` +
			`\n使用示例：\n` +
			`#派蒙语音生成你好\n` +
			`#派蒙语音你好\n` +
			`#胡桃一言\n` +
			`#语音语速设置10\n` +
			`\n注：不支持别名，请输入准确的名字\n` +
			`崩三角色语音全部失效`
		let speakersort = speakermap.split(",");
		let speakertip = "发言者列表：\n";
		for (let i = 0; i < speakersort.length; i++) {
			speakertip += speakersort[i];
			if (i % 2 == 0) speakertip += "　　";
			else speakertip += "\n";
		}
		let msgx = await common.makeForwardMsg(e, [msg, tip, speakertip], `语音生成帮助`);
		e.reply(msgx);
		return true;
	}

	async detectspeak(speakername) {
		let speakersort = speakermap.split(",");
		for (let i = 0; i < speakersort.length; i++) {
			if (speakersort[i] == speakername) return 1;
		}
		return 0;
	}
}