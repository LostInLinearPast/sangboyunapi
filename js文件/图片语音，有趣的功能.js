import plugin from '../../lib/plugins/plugin.js';
import {
	segment
} from "oicq";
import cfg from '../../lib/config/config.js';
import common from '../../lib/common/common.js';
import fetch from 'node-fetch';
import fs from "fs";
import moment from "moment"

//桑帛api官网：https://api.lolimi.cn/
const key = ''
//预留项

//对此插件有疑问，请联系作者（qq）：1283411677

//注：语音的功能需要配置ffmpeg，不懂什么意思，那你就当做用不了

//下方仅标记“可用”的内容才可用，未标记都为不可用

export class sangboapi extends plugin {
	constructor() {
		super({
			name: '桑帛api',
			dsc: '一些好玩的',
			event: 'message',
			priority: 999,
			rule: [{
					reg: `^#?(随机)?猫猫(图)?$`, 
					fnc: 'chaijunpic'   //可用
				},
				{
					reg: `^#(随机)?原神图片?$`,  
					fnc: 'genshinpic'   //可用
				},
				{
					reg: `^#(卢本伟|大司马|药水哥|周淑怡|pdd|PDD|王司徒|乔碧萝|金坷垃|郭老师|冬泳怪鸽|旭旭宝宝|电棍|茄子|哲学|JOJO|jojo|老八|丁真)((随机)?语音)?$`,
					fnc: 'voiceapi'     
				},
				{
					reg: `^#(随机)?漫剪$`,
					fnc: 'acgvideo'     
				},
				{
					reg: `^#(随机)?动漫图?$`,
					fnc: 'randomacg'    
				},
				{
					reg: `^#(随机)?美女图?$`,
					fnc: 'randombeauty' //可用
				},
				{
					reg: `^#(随机)?白丝(美女)?(图)?$`,
					fnc: 'randomwhite'
				},
				{
					reg: `^#(随机)?黑丝(美女)?(图)?$`,
					fnc: 'randomblack'
				},
				{
					reg: `^#(.*)点阵字(.*)$`,
					fnc: 'bigword'
				},
				{
					reg: `^#(颜值)?(打|评)分(.*)$`,
					fnc: 'detectpic'
				},
				{
					reg: `^#(只(音|因)|小黑子音|(kun|坤)(kun|坤)?音?|鸡|打(kun|坤))乐$`,
					fnc: 'kunvoice'
				},
				{
					reg: `^#日期$`,
					fnc: 'calendar'
				},
				{
					reg: `^#(((今|每)日)?60秒|看?新闻|(今日)?日报|发生(甚|什)么事了|看报)$`,
					fnc: 'daynews'
				},
				{
					reg: `^#二次元的(.*)$`,
					fnc: 'quadraticu'
				},
				{
					reg: `^#(桑帛|sb|Sb|SB|sB|s13|S13|桑鸡|桑|桑帛api)帮助$`,
					fnc: 'sbhelp'
				},
				{
					reg: `^#(随机)?语音(列表)?$`,
					fnc: 'voicelist'
				},
			]
		})
	}

	async genshinpic(e) {
		let url = 'https://api.lolimi.cn/API/yuan/?type=text'
        let res = await fetch(url);
        res = await res.json();
		e.reply(segment.image(res));
		return true
	}

	async chaijunpic(e) {
		let url = 'https://api.lolimi.cn/API/chaiq/c.php'
		e.reply(segment.image(url));
		return true
	}

	async voiceapi(e) {
		let msg = e.msg.replace(/语音|随机语音/, '')
			.replace(/#/, '')
		if (msg == 'jojo') msg = 'JOJO'
		if (msg == 'pdd') msg = 'PDD'
		let url = 'https://api.lolimi.cn/api/yyb/v?msg=' + msg + '&key=' + key
		e.reply(segment.record(url));
		return true
	}

	async acgvideo(e) {
		e.reply('请稍等片刻哦~')
		let url = 'https://api.lolimi.cn/API/shiping/mj.php'
		e.reply(segment.video(url));
		return true;
	}

	async randomacg(e) {
		let url = 'https://api.lolimi.cn/api/dm/index?key=' + key
		e.reply(segment.image(url));
		return true
	}

	async randombeauty(e) {
		let url = 'https://api.lolimi.cn/API/tup/xjj.php'
		e.reply(segment.image(url));
		return true
	}

	async randomwhite(e) {
		let url = 'https://api.lolimi.cn/api/bhs/b?key=' + key
		e.reply(segment.image(url));
		return true
	}

	async randomblack(e) {
		let url = 'https://api.lolimi.cn/api/bhs/h?key=' + key
		e.reply(segment.image(url));
		return true
	}

	async bigword(e) {
		let msg = e.msg.replace(/^#(.*)点阵字/, '')
		let fill = e.msg.replace(/^#?/, '')
			.replace(/点阵字(.*)$/, '')
		if (fill == '') fill = '■'
		let url = `https://api.lolimi.cn/api/dzz/d?msg=${msg}&key=${key}`;
		let res = await fetch(url)
			.catch((err) => logger.error(err));
		res = await res.json();
		if (fill != '■') res.msg = res.msg.replace(/■/g, fill)
			.replace(/□/g, '　');
		let msgx = await common.makeForwardMsg(e, [`点阵字：${msg}`, `填充字符：${fill}`, res.msg], `点阵字`)
		e.reply(msgx)
		return true;
	}

	async detectpic(e) {
		if (!e.img) {
			this.setContext('_detectpicContext')
			e.reply('⚠ 请发送图片')
			return;
		}
		if (!e.img) return;
		let picurl = e.img[0]
		let restext = '';
		let url = `https://api.lolimi.cn/api/yanz/y?url=${picurl}&key=${key}`
		let res = await fetch(url)
			.catch((err) => logger.error(err));
		res = await res.json();
		if (res.code == -7) {
			restext = res.text;
			e.reply(restext);
			return true;
		} else if (res.code == 1) {
			restext = res.data.rep_image;
			e.reply(segment.image(restext));
			return true;
		}
	}

	async _detectpicContext() {
		let img = this.e.img
		if (/取消/.test(this.e.msg)) {
			this.finish('_detectpicContext')
			await this.reply('✅ 已取消')
			return
		}
		if (!img) {
			this.setContext('_detectpicContext')
			await this.reply('⚠ 请发送图片或取消')
			return
		}
		let picurl = img[0]
		let restext = '';
		let url = `https://api.lolimi.cn/api/yanz/y?url=${picurl}&key=${key}`
		let res = await fetch(url)
			.catch((err) => logger.error(err));
		res = await res.json();
		if (res.code == -7) {
			restext = res.text;
			this.e.reply(restext);
		} else if (res.code == 1) {
			restext = res.data.rep_image;
			this.e.reply(segment.image(restext));
		}

		this.finish('_detectpicContext')
		return true;
	}

	async kunvoice(e) {
		let url = 'https://api.lolimi.cn/api/kunkun/k?key=' + key
		e.reply(segment.record(url));
		return true
	}

	async calendar(e) {
		let url = `http://api.lolimi.cn/api/ri/li?key=${key}`
		e.reply(segment.image(url));
		return true
	}

	async daynews(e) {
		let url = `https://api.lolimi.cn/api/60/index?key=${key}&qq=${e.user_id}`
		e.reply(segment.image(url));
		return true
	}

	async quadraticu(e) {
		let name;
		if (e.msg.replace(/^#二次元的/, '') == "我") name = e.sender.card
		else if (e.msg.replace(/^#二次元的/, '') == "") return false;
		else {
			for (var i in e.message) {
				if (e.message[i].type == 'at')
					name = e.message[i].text.replace(/^@/, '')
			}
		}
		var i = Math.round(Math.random() * 100)
		name = name + "Π" + i + "Π";
		let url = `https://api.lolimi.cn/API/Ser/?name=${name}&type=json`
		let res = await fetch(url)
			.catch((err) => logger.error(err));
		res = await res.json();
		let replyres = res.text.replace(/Π(.*)Π/g, '')
		e.reply(replyres);
		return true;
	}

	async sbhelp(e) {
		let msg = `目前所有api功能如下：\n` +
			`#(随机)原神图\n` +
			`#(随机)语音\n` +
			`#(随机)漫剪\n` +
			`#(随机)动漫图\n` +
			`#(随机)美女图\n` +
			`#(随机)白丝(美女图)\n` +
			`#(随机)黑丝(美女图)\n` +
			`(#)(随机)猫猫(图)\n` +
			`#(x)点阵字xxx\n` +
			`#(颜值)打分\n` +
			`#只音乐\n` +
			`#日期\n` +
			`#二次元的[我/@]\n` +
			`#(每日)60秒`
		let tip = `()：表示可以不加\n例如 ：\n"#随机漫剪" = "#漫剪"`
		let tip2 = `x：表示一个字\nxxx：表示一个字以上`
		let tip3 = `[]：需要直接忽略\n/：表示“或”\n@：表示艾特某人`
		let msgx = await common.makeForwardMsg(e, [msg, tip, tip2, tip3], `一些好玩的喵~`)
		e.reply(msgx)
		return true
	}

	async voicelist(e) {
		let msg = `目前所有语音列表如下：\n` +
			`卢本伟\n` +
			`大司马\n` +
			`药水哥\n` +
			`周淑怡\n` +
			`pdd\n` +
			`王司徒\n` +
			`乔碧萝\n` +
			`金坷垃\n` +
			`郭老师\n` +
			`冬泳怪鸽\n` +
			`旭旭宝宝\n` +
			`电棍\n` +
			`茄子\n` +
			`哲学\n` +
			`jojo\n` +
			`老八\n` +
			`丁真\n`
		let tip = `使用例子：“#卢本伟语音”`
		let tip2 = `注：错别字是无法识别的`
		let msgx = await common.makeForwardMsg(e, [msg, tip, tip2], `可用的随机语音喵~`)
		e.reply(msgx)
		return true
	}
}