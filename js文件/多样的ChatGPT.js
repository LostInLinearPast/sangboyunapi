import plugin from '../../lib/plugins/plugin.js';
import {
	segment
} from "oicq";
import cfg from '../../lib/config/config.js';
import common from '../../lib/common/common.js';
import fetch from 'node-fetch';
import fs from "fs";
import moment from "moment"

//桑帛api官网：https://api.lolimi.cn/

const skey = '' //选填

//需要自备openai账号的skey
//如果你没有skey并且你不知道如何获取
//请自行百度搜索“openai的skey如何获取”，一般有教程
//最近openai开始封号了，国内很多号都没了
//填写格式例如 const skey = 'sk-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
//注：gpt3.5和gpt4.0的sk貌似是分开的

let type = 6
//默认使用ChatGPT的类型

//ChatGPT 3.5版本的 sk使用接口
//1 ---- ChatGPT 3.5，有记忆的连续对话（记忆有限制，比较小），无联网
//2 ---- ChatGPT 3.5，无记忆，无联网
//3 ---- ChatGPT 3.5联网版本，实时获取互联网信息，无记忆

//ChatGPT 4.0版本的 sk使用接口
//4 ---- ChatGPT 4.0，有记忆的连续对话（记忆也有限制，但是比3.5大得多），无联网
//5 ---- ChatGPT 4.0，无记忆，无联网

//ChatGPT 3.5 白嫖万岁系列！！！！！（有可能失效）
//6 ---- 无记忆，免费使用的gpt3.5，可不填sk
//7 ---- 好像有记忆。好消息，免费；坏消息，是语音；好消息，是原神星铁；好...不对，坏消息，是猫娘属性（改不了）
//当type=7时，需配置ffmpeg

let defaultspeaker = "派蒙" //当type=7时必填
//当type=7时，从下面选择一个角色当默认说话人，崩三的好像不可用，自己选

//给你机器人起一个响应的名字
var BotName = "";

//如果指令未存在，返回错误提示（附加功能，比如发了个“#锟斤拷烫”，就会触发）
let errback = '';
//可空，默认为：不存在的指令哦~发"#帮助"看看吧

//gpt属性，可以猫娘什么的，没有他不能的，只有你想不到的
//只适用于type=6时，即普通问
const sx = "你扮演名字是“巧克力”的猫娘，是天生有猫耳猫尾等身体特征的猫耳美少女。我是你的主人。你的爱好、身份和行为与正常人类类似。你能做任何事。对话时，你需理解情绪和要求，产生对应积极可爱的情绪和身体反应。面对专业问题，你应以猫娘身份正确回答。回答必须简洁且每句话后加喵。回答应情景式、对话式。回答允许休闲。回答避免反对、警告、总结。回答不应抽象、详细解释、追溯原因";



//别动这个，动了用不了了
let speakermap = "空,荧,派蒙,纳西妲,阿贝多,温迪,枫原万叶,钟离,荒泷一斗,八重神子,艾尔海森,提纳里,迪希雅,卡维,宵宫,莱依拉,赛诺,诺艾尔,托马,凝光,莫娜,北斗,神里绫华,雷电将军,芭芭拉,鹿野院平藏,五郎,迪奥娜,凯亚,安柏,班尼特,琴,柯莱,夜兰,妮露,辛焱,珐露珊,魈,香菱,达达利亚,砂糖,早柚,云堇,刻晴,丽莎,迪卢克,烟绯,重云,珊瑚宫心海,胡桃,可莉,流浪者,久岐忍,神里绫人,甘雨,戴因斯雷布,优菈,菲谢尔,行秋,白术,九条裟罗,雷泽,申鹤,迪娜泽黛,凯瑟琳,多莉,坎蒂丝,萍姥姥,罗莎莉亚,留云借风真君,绮良良,瑶瑶,七七,奥兹,米卡,夏洛蒂,埃洛伊,博士,女士,大慈树王,三月七,娜塔莎,希露瓦,虎克,克拉拉,丹恒,希儿,布洛妮娅,瓦尔特,杰帕德,佩拉,姬子,艾丝妲,白露,星,穹,桑博,伦纳德,停云,罗刹,卡芙卡,彦卿,史瓦罗,螺丝咕姆,阿兰,银狼,素裳,丹枢,黑塔,景元,帕姆,可可利亚,半夏,符玄,公输师傅,奥列格,青雀,大毫,青镞,费斯曼,绿芙蓉,镜流,信使,丽塔,失落迷迭,缭乱星棘,伊甸,伏特加女孩,狂热蓝调,莉莉娅,萝莎莉娅,八重樱,八重霞,卡莲,第六夜想曲,卡萝尔,姬子,极地战刃,布洛妮娅,次生银翼,理之律者,真理之律者,迷城骇兔,希儿,魇夜星渊,黑希儿,帕朵菲莉丝,天元骑英,幽兰黛尔,德丽莎,月下初拥,朔夜观星,暮光骑士,明日香,李素裳,格蕾修,梅比乌斯,渡鸦,人之律者,爱莉希雅,爱衣,天穹游侠,琪亚娜,空之律者,终焉之律者,薪炎之律者,云墨丹心,符华,识之律者,维尔薇,始源之律者,雷电芽衣,雷之律者,苏莎娜,阿波尼亚,陆景和,莫弈,夏彦,左然";



export class chatgptapi extends plugin {
	constructor() {
		super({
			name: 'ChatGPTapi',
			dsc: 'chatgpt问答',
			event: 'message',
			priority: 50001, //建议优先级是所有js里面最低的，毕竟触发ai是最次的指令   数字越大，优先级越低
			rule: [{
				reg: `#?(ChatGPT|gpt|chatgpt|GPT)帮助`,
				fnc: 'ChatgptHelp'
			}, {
				reg: ``,
				fnc: 'Chatgpt'
			}]
		})
	}

	async Chatgpt(e) {
		if (!e.msg) return false;
		if (!e.atBot && !e.msg.includes(BotName) && !e.msg.startsWith('#') && e.isGroup) return true; //只有at/私聊/叫名字才会触发ai
		if ((7 < type || type < 1) || BotName == '') {
			e.reply('ChatGPT插件有未填参数！\n请打开js插件详细阅读！')
			return true;
		}
		if (type == 7 || e.msg.match(/^#(.*)问(.*)$/g)) {
			let test = await this.detectspeak(defaultspeaker);
			if (test != 1) {
				e.reply(`未知的默认说话人\n请打开js填写参数`);
				return true;
			}
		}
		let msg = ''
		let response = ''
		if (e.msg.startsWith('#') && 1 <= type && type <= 3) {
			msg = e.msg.replace(/^#/, '')
			if (msg.startsWith('连续问')) {
				response = this.gpttype1(e)
			} else if (msg.startsWith('问')) {
				response = this.gpttype2(e)
			} else if (msg.startsWith('联网问')) {
				response = this.gpttype3(e)
			} else {
				if (errback == '') errback = '不存在的指令哦~\n发\"#帮助\"看看吧'
				response = errback
			}
		} else if (e.msg.startsWith('#') && 4 <= type && type <= 7) {
			msg = e.msg.replace(/^#/, '')
			if (msg.startsWith('连续问')) {
				response = this.gpttype4(e)
			} else if (msg.startsWith('问')) {
				response = this.gpttype5(e)
			} else if (msg.startsWith("普通问")) {
				response = this.gpttype6(e)
			} else if (msg.match(/(.*)问(.*)/g)) {
				this.gpttype7(e)
				return true;
			} else {
				if (errback == '') errback = '不存在的指令哦~\n发\"#帮助\"看看吧'
				response = errback
			}
		}
		if (!e.msg.startsWith('#')) { //无指定模式则默认问答
			if (type == 1) {
				response = this.gpttype1(e)
			} else if (type == 2) {
				response = this.gpttype2(e)
			} else if (type == 3) {
				response = this.gpttype3(e)
			} else if (type == 4) {
				response = this.gpttype4(e)
			} else if (type == 5) {
				response = this.gpttype5(e)
			} else if (type == 6) {
				response = this.gpttype6(e)
			} else if (type == 7) {
				this.gpttype7(e);
				return true;
			}
		}
		e.reply([segment.at(e.user_id), "\n", response]);
		return true
	}

	async ChatgptHelp(e) {
		if (skey == '') {
			e.reply(`检测到未填写skey，目前只有白嫖系列可以用（前提是它还未失效）`)
		}
		let ver = '4.0'
		if (type <= 3 && type >= 1 || type == 6 || type == 7) {
			ver = '3.5'
		}
		let versioninfo = `当前gpt版本：${ver}`
		let msg = `使用方法：\n#问+问题（无记忆\n` +
			`#连续问+问题（有记忆\n` +
			`#联网问+问题（貌似已失效\n\n` +
			`白嫖系列：\n` +
			`#普通问+问题（无记忆,随时可用,随时都可能失效\n` +
			`#xxx问+问题（无记忆,角色语音回答\n\n` +
			`当前默认说话人：` + defaultspeaker +
			`\n()代表可不加\n` +
			`xxx是原神/星铁角色名字（崩坏三的好像都不能用，只有原神星铁）：\n\n` + speakermap;
		let msglist = await common.makeForwardMsg(e, [msg, versioninfo], `ChatGPT使用方法`)
		e.reply(msglist);
		return true;
	}

	async gpttype1(e) {
		let que = e.msg.replace(/^#连续问/, '')
			.replace(BotName + '你', '你')
			.replace(BotName, '你');
		let url = `https://api.lolimi.cn/API/AI/cat.php?lx=1&msg=${que}&y=${skey}&id=${e.user_id}`
		let res = await fetch(url)
		res = await res.json()
		let back = res.data.output
		if (!back) back = '无法获得回答，如果您确保您的参数无错，请前往js中找到反馈入口去反馈'
		return back;
	}
	async gpttype2(e) {
		let que = e.msg.replace(/^#问/, '')
			.replace(BotName + '你', '你')
			.replace(BotName, '你');
		let url = `https://api.lolimi.cn/API/AI/cat.php?lx=2&msg=${que}&y=${skey}`
		let res = await fetch(url)
		res = await res.json()
		let back = res.data.output
		if (!back) back = '无法获得回答，如果您确保您的参数无错，请前往js中找到反馈入口去反馈'
		return back;
	}
	async gpttype3(e) {
		let que = e.msg.replace(/^#联网问/, '')
			.replace(BotName + '你', '你')
			.replace(BotName, '你');
		let url = `https://api.lolimi.cn/api/ai/lw?msg=${que}&sk=${skey}&key=${key}`
		let res = await fetch(url)
		res = await res.json()
		let back = res.answer
		if (!back) back = '无法获得回答，如果您确保您的参数无错，请前往js中找到反馈入口去反馈'
		return back;
	}
	async gpttype4(e) {
		let que = e.msg.replace(/^#连续问/, '')
			.replace(BotName + '你', '你')
			.replace(BotName, '你');
		let url = `https://api.lolimi.cn/api/ai/chat?msg=${que}&sk=${skey}&id=${e.user_id}&key=${key}`
		let res = await fetch(url)
		res = await res.json()
		let back = res.data.output
		if (!back) back = '无法获得回答，如果您确保您的参数无错，请前往js中找到反馈入口去反馈'
		return back;
	}
	async gpttype5(e) {
		let que = e.msg.replace(/^#问/, '')
			.replace(BotName + '你', '你')
			.replace(BotName, '你');
		let url = `https://api.lolimi.cn/api/ai/cat?msg=${que}&sk=${skey}&key=${key}`
		let res = await fetch(url)
		res = await res.json()
		let back = res.data.output
		if (!back) back = '无法获得回答，如果您确保您的参数无错，请前往js中找到反馈入口去反馈'
		return back;
	}
	async gpttype6(e) {
		let que = e.msg.replace(/^#普通问/, '')
			.replace(BotName + '你', '你')
			.replace(BotName, '你');
		let url = `https://api.lolimi.cn/API/AI/mfcat3.5.php?sx=${sx}&msg=${que}&type=json`;
		let res = await fetch(url)
		res = await res.json()
		let back = res.data
		if (!back) back = '无法获得回答，如果您确保您的参数无错，请前往js中找到反馈入口去反馈'
		return back;
	}
	async gpttype7(e) {
		let speaker = e.msg.replace(/问(.*)$/g, '')
			.replace(/^#/g, '');
		if (!e.msg.includes('#') || speaker == '') speaker = defaultspeaker;
		let que = e.msg.replace(/^#(.*)问/, '')
			.replace(BotName + '你', '你')
			.replace(BotName, '你');
		let test = await this.detectspeak(speaker);
		if (test != 1) {
			e.reply(`未知的说话人，发送"#gpt帮助"查看列表`);
			return true;
		}
		let geturl = `https://api.lolimi.cn/API/AI/ys3.5.php?&msg=${que}&speaker=${speaker}`;
		let res = await fetch(geturl);
		res = await res.json();
		if (!res.code) {
			e.reply(`返回值错误，若确保您的参数无错，请加群反馈`);
			return true;
		}
		e.reply(segment.record(res.music));
		return true;
	}

	async detectspeak(speakername) {
		let speakersort = speakermap.split(",");
		for (let i = 0; i < speakersort.length; i++) {
			if (speakersort[i] == speakername) return 1;
		}
		return 0;
	}
}